#pragma once

#include "ldupes/atomics.h"
#include "ldupes/ld_error.h"

#include <memory>
#include <string>
#include <vector>

namespace ldupes {
    struct Error {
        Error(ld_error &&error);
        Error(Error const &) = delete;
        Error(Error &&)      = default;

        ~Error() { free(message); }

        bool ok() const { return type == Type::OK; }

        bool cancelled() const { return type == Type::CANCELLED; }

        enum Type {
            OK,
            CANT_ACCESS,
            NOT_DIRECTORY,
            OOM,
            EOI,
            HASHING_ERROR,
            CANCELLED,
        } type;

        char *message = nullptr;
    };

    class Context {
      public:
        Context(std::string dirname);
        Context(std::string dirname, atomic_bool &cancelled);

        ~Context();

        void set_min_file_size(size_t size_in_bytes);

        Error next_duplicate();

        std::size_t get_one_duplicate_size() const { return file_size; }

        std::vector<std::string> get_duplicates_group() { return duplicates_group; }

      private:
        struct Impl;
        std::unique_ptr<Impl> pimpl;
        std::size_t file_size;
        std::vector<std::string> duplicates_group;
    };
} // namespace ldupes
