#include "ldupes.hpp"

#include "ldupes/ldupes.h"

#include <cassert>

namespace ldupes {
    Error::Error(ld_error &&error) {
        switch (error.type) {
        case ld_error::ld_ERR_OK:
            type = Type::OK;
            break;
        case ld_error::ld_ERR_CANT_ACCESS:
            type    = Type::CANT_ACCESS;
            message = error.message;
            break;
        case ld_error::ld_ERR_NOT_DIRECTORY:
            type = Type::NOT_DIRECTORY;
            break;
        case ld_error::ld_ERR_OUT_OF_MEMORY:
            type = Type::OOM;
            break;
        case ld_error::ld_ERR_END_OF_ITERATION:
            type = Type::EOI;
            break;
        case ld_error::ld_ERR_HASHING_ERROR:
            type = Type::HASHING_ERROR;
            break;
        case ld_error::ld_ERR_CANCELLED:
            type = Type::CANCELLED;
            break;
        default:
            assert(false && "unknown error");
        }
    }

    struct context_list_deleter {
        void operator()(ld_ranked_list *list) {
            LD_RANKED_LIST_CLEAR(list);
        }
    };
    using context_list_ptr = std::unique_ptr<ld_ranked_list, context_list_deleter>;

    struct Context::Impl {
        Impl(std::string dirname, atomic_bool *cancelled) : dirname(std::move(dirname)) {
            ld_context_init(&context, this->dirname.c_str());
            context.cancelled = cancelled;
        }

        ~Impl() {
            ld_context_clear(&context);
        }

        std::string const dirname;
        ld_context context;
    };

    Context::Context(std::string dirname)
        : pimpl(std::make_unique<Context::Impl>(std::move(dirname), nullptr)) {
    }

    Context::Context(std::string dirname, atomic_bool &cancelled)
        : pimpl(std::make_unique<Context::Impl>(std::move(dirname), &cancelled)) {
    }

    Context::~Context() {}

    void Context::set_min_file_size(size_t size_in_bytes) {
        pimpl->context.min_file_size = size_in_bytes;
    }

    Error Context::next_duplicate() {
        Error status = Error(ld_next_duplicate(&pimpl->context));
        if (status.ok() or status.cancelled()) {
            duplicates_group.clear();
            context_list_ptr list_ptr(&pimpl->context.dups_list, context_list_deleter{});
            file_size = list_ptr->file_size;
            ld_ranked_list_entry *it;
            SLIST_FOREACH(it, list_ptr.get(), entries) {

